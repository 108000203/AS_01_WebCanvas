
var canvas;
var context;
var imageStack;
var blankCanvas;
// Mode List
var mode = ['NONE','BRUSH','ERASER', 'TEXT', 'CIRCLE', 'SQUARE', 'TRIANGLE'];
// set element after it completely loaded 
window.onload = function(){
    console.log('canvas!!');
    canvas = document.getElementById('background');
    context = canvas.getContext('2d');
    undoStack = new Array();
    redoStack = new Array();
    blankCanvas = context.getImageData(0, 0, canvas.width, canvas.height);
} 

// select Mode
var isDrawing;
var textpositionX;
var textpositionY;
function select_mode(Mode){
    
    console.log('select_mode: ' + mode[Mode]);
    let x = 0;
    let y = 0;
    //判定前次指令未完成
    
    if(isDrawing === true) {
        console.log('something go wrong, maybe is the text input not finish');
        undoStack.pop();
        tx.remove();
        
    }
    
    isDrawing = false;
    var interval; // for square, circle, triangle
    var flip = 1;
    // 清除EventListener
    try{
        canvas.removeEventListener('mousedown', mouse_down);
        canvas.removeEventListener('mousemove', mouse_move);
        window.removeEventListener('mouseup', mouse_up);
        console.log('remove listener');
    }catch{
        console.log('fisrt listener invoke');
    }
    
    
    mouse_down = function(e) {
        save_point();


        if(mode[Mode] == 'BRUSH'){
            
            x = e.offsetX;
            y = e.offsetY;
            isDrawing = true;
            
        } else if (mode[Mode] == 'ERASER'){
            x = e.offsetX;
            y = e.offsetY;
            isDrawing = true;
            //
            context.globalCompositeOperation="destination-out";
            
            //save_point();
        } else if (mode[Mode] == 'TEXT'){
            if(isDrawing === true){ 
                console.log('not enter text yet'); 
                undoStack.pop();
                undoStack.pop();
                tx.remove();
                isDrawing = false;
            }
            else {
                console.log('create a text box.');
                textpositionX = e.clientX;
                textpositionY = e.clientY;
                create_TextBox(e.clientX, e.clientY);
                isDrawing = true;
            }
            
            
        } else if (mode[Mode] == 'CIRCLE'){
            
            console.log('md-cricle');
            x0 = e.offsetX;
            y0 = e.offsetY;
            display_circle(x0, y0);
            isDrawing = true;

        } else if (mode[Mode] == 'SQUARE'){
            console.log('md-square');
            x0 = e.offsetX;
            y0 = e.offsetY;
            display_square(x0, y0);
            isDrawing = true;

        } else if (mode[Mode] == 'TRIANGLE'){
            console.log('md-triangle');
            x0 = e.offsetX;
            y0 = e.offsetY;
            display_triangle(x0, y0);
            isDrawing = true;
        }
        
    }
    mouse_move = function(e) {
        if(mode[Mode] == 'BRUSH'){   
            if (isDrawing === true) {
                //console.log('mousemove-brush');
                drawLine(context, x, y, e.offsetX, e.offsetY);
                x = e.offsetX;
                y = e.offsetY;
            }
        } else if(mode[Mode] == 'ERASER'){   
            if (isDrawing === true) {
                //console.log('mousemove-erase');
                //eraseLine(context, x, y, e.offsetX, e.offsetY);
                drawLine(context, x, y, e.offsetX, e.offsetY);
                x = e.offsetX;
                y = e.offsetY;
            }
        } else if(mode[Mode] == 'TEXT'){
            if (isDrawing === true){
                if(x > canvas.width || y > canvas.height){
                    console.log('text out of cursor');
                    isDrawing = false;
                }
                x = e.offsetX;
                y = e.offsetY;
                
            }
        } else if (mode[Mode] == 'CIRCLE'){
            if(isDrawing === true){

                //ccSize = document.getElementById('circle');
                quadrant = 0;
                var ccSize = document.getElementById('circle');
                var ccSizeW = document.getElementById('circle').style.width;
                var ccSizeH = document.getElementById('circle').style.height;
                
                // in Quadrant 4
                if((x-x0) >= 0 && (y-y0) >= 0){
                    //console.log('in Q4');
                    quadrant = 4;

                    ccSize.style.bottom = "";
                    document.getElementById('circle').style.top = (y0) + "px";

                    ccSize.style.right = "";
                    document.getElementById('circle').style.left = (x0) + "px";
                    
                    document.getElementById('circle').style.width = (x - x0) + "px";
                    document.getElementById('circle').style.height = (y - y0) + "px";  
                }
                // in Quadrant 3
                else if((x-x0) < 0 && (y-y0) >= 0){
                    //change left -> right 
                    //console.log('in Q3');
                    quadrant = 3;

                    ccSize.style.left = "";
                    console.log( "here " + document.body.clientWidth);
                    document.getElementById('circle').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ccSize.style.bottom = "";
                    document.getElementById('circle').style.top = (y0) + "px";

                    
                    document.getElementById('circle').style.width = (x0 - x) + "px";
                    document.getElementById('circle').style.height = (y - y0) + "px";
                }
                //in Quadrant 2
                else if((x-x0) < 0 && (y-y0) < 0){
                    quadrant = 2;

                    ccSize.style.left = "";
                    document.getElementById('circle').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ccSize.style.top = "";
                    console.log(document.body.clientHeight);
                    //console.log(y0);
                    document.getElementById('circle').style.bottom = (document.documentElement.clientHeight- y0) + "px"; // 90 is th title height
                    
                    document.getElementById('circle').style.width = (x0 - x) + "px";
                    document.getElementById('circle').style.height = (y0 - y) + "px";
                }
                // in in Quadrant 1
                else if((x-x0) >= 0 && (y-y0) < 0){
                    //console.log('in Q1');
                    quadrant = 1;
                    ccSize.style.top = "";
                    document.getElementById('circle').style.bottom = (document.documentElement.clientHeight - y0) + "px";

                    ccSize.style.right = "";
                    document.getElementById('circle').style.left = (x0) + "px";
                    
                    document.getElementById('circle').style.width = (x - x0) + "px";
                    document.getElementById('circle').style.height = (y0 - y) + "px";
                }             
                
            }
            // cursor pos in canvas update
            x = e.offsetX;
            y = e.offsetY;


        } else if (mode[Mode] == 'SQUARE'){
            if(isDrawing === true){
                quadrant = 0;
                var ccSize = document.getElementById('square');
                var ccSizeW = document.getElementById('square').style.width;
                var ccSizeH = document.getElementById('square').style.height;
                
                // in Quadrant 4
                if((x-x0) >= 0 && (y-y0) >= 0){
                    //console.log('in Q4');
                    quadrant = 4;

                    ccSize.style.bottom = "";
                    document.getElementById('square').style.top = (y0) + "px";

                    ccSize.style.right = "";
                    document.getElementById('square').style.left = (x0) + "px";
                    
                    document.getElementById('square').style.width = (x - x0) + "px";
                    document.getElementById('square').style.height = (y - y0) + "px";  
                }
                // in Quadrant 3
                else if((x-x0) < 0 && (y-y0) >= 0){
                    quadrant = 3;

                    ccSize.style.left = "";
                    
                    document.getElementById('square').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ccSize.style.bottom = "";
                    document.getElementById('square').style.top = (y0) + "px";

                    
                    document.getElementById('square').style.width = (x0 - x) + "px";
                    document.getElementById('square').style.height = (y - y0) + "px";
                }
                //in Quadrant 2
                else if((x-x0) < 0 && (y-y0) < 0){
                    quadrant = 2;

                    ccSize.style.left = "";
                    document.getElementById('square').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ccSize.style.top = "";
                    console.log(document.body.clientHeight);

                    document.getElementById('square').style.bottom = (document.documentElement.clientHeight - y0) + "px"; // 90 is th title height
                    
                    document.getElementById('square').style.width = (x0 - x) + "px";
                    document.getElementById('square').style.height = (y0 - y) + "px";
                }
                // in in Quadrant 1
                else if((x-x0) >= 0 && (y-y0) < 0){
                    quadrant = 1;

                    ccSize.style.top = "";
                    //console.log(document.documentElement.clientHeight);
                    document.getElementById('square').style.bottom = (document.documentElement.clientHeight - y0) + "px";

                    ccSize.style.right = "";
                    document.getElementById('square').style.left = (x0) + "px";
                    
                    document.getElementById('square').style.width = (x - x0) + "px";
                    document.getElementById('square').style.height = (y0 - y) + "px";
                }             
                
            }
            // cursor pos in canvas update
            x = e.offsetX;
            y = e.offsetY;


        }
        else if (mode[Mode] == "TRIANGLE"){
            if(isDrawing === true){
                quadrant = 0;
                var ttSize = document.getElementById('triangle');
                //var ttSizeW = document.getElementById('triangle').style.width;
                //var ttSizeH = document.getElementById('triangle').style.height;
                
                // in Quadrant 4
                if((x-x0) >= 0 && (y-y0) >= 0){
                    console.log('in Q4');
                    quadrant = 4;

                    ttSize.style.bottom = "";
                    document.getElementById('triangle').style.top = (y0) + "px";

                    ttSize.style.right = "";
                    document.getElementById('triangle').style.left = (x0) + "px";
                    
                    document.getElementById('triangle').style.borderLeft = (x - x0)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderRight = (x - x0)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderBottom = (y - y0) + "px solid " + document.getElementById('Color').value;
                }
                // in Quadrant 3
                else if((x-x0) < 0 && (y-y0) >= 0){
                    quadrant = 3;

                    ttSize.style.left = "";
                    document.getElementById('triangle').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ttSize.style.bottom = "";
                    document.getElementById('triangle').style.top = (y0) + "px";

                    document.getElementById('triangle').style.borderLeft = (x0 - x)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderRight = (x0 - x)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderBottom = (y - y0) + "px solid " + document.getElementById('Color').value;
                }
                //in Quadrant 2
                else if((x-x0) < 0 && (y-y0) < 0){
                    quadrant = 2;

                    ttSize.style.left = "";
                    document.getElementById('triangle').style.right = (document.documentElement.clientWidth - x0) + "px";

                    ttSize.style.top = "";
                    console.log(document.body.clientHeight);

                    document.getElementById('triangle').style.bottom = (document.documentElement.clientHeight - y0) + "px"; // 90 is th title height
                    
                    document.getElementById('triangle').style.borderLeft = (x0 - x)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderRight = (x0 - x)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderBottom = (y0 - y) + "px solid " + document.getElementById('Color').value;
                }
                // in in Quadrant 1
                else if((x-x0) >= 0 && (y-y0) < 0){
                    quadrant = 1;

                    ttSize.style.top = "";
                    document.getElementById('triangle').style.bottom = (document.documentElement.clientHeight - y0) + "px";

                    ttSize.style.right = "";
                    document.getElementById('triangle').style.left = (x0) + "px";
                    //
                    document.getElementById('triangle').style.borderLeft = (x - x0)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderRight = (x - x0)/2 + "px solid transparent";
                    document.getElementById('triangle').style.borderBottom = (y0 - y) + "px solid " + document.getElementById('Color').value;
                }             
                
            }
            // cursor pos in canvas update
            x = e.offsetX;
            y = e.offsetY;
            //x = document.body.clientX;
            //y = document.body.clientY;

        }
    }
    
 
    mouse_up = function(e) {
        if(mode[Mode] == 'BRUSH'){
            
            if (isDrawing === true) {
                //console.log('mouseup-brush');
                drawLine(context, x, y, e.offsetX, e.offsetY);
                x = 0;
                y = 0;
                isDrawing = false;
            }            
        } else if (mode[Mode] == 'ERASER'){
            if (isDrawing === true) {
                //console.log('mouseup-earse');
                //eraseLine(context, x, y, e.offsetX, e.offsetY);
                drawLine(context, x, y, e.offsetX, e.offsetY);
                context.globalCompositeOperation="source-over";
                x = 0;
                y = 0;
                isDrawing = false;
            }    
        } else if (mode[Mode] == 'CIRCLE'){
            if(isDrawing === true){
                context.beginPath();
                if(quadrant == 1){
                    //string.match(/\d+/g).map(Number);
                   
                    centerX = (parseInt((document.getElementById('circle').style.left),10) + parseInt(x, 10))/2.;
                    centerY = (parseInt(y0,10) + parseInt(y, 10))/2 ;
                    radiusX = (parseInt((document.getElementById('circle').style.width),10))/2;
                    radiusY = (parseInt(document.getElementById('circle').style.height))/2;
                    
                } else if (quadrant == 2) {
                    centerX = (document.documentElement.clientWidth - parseInt((document.getElementById('circle').style.right),10) + parseInt(x, 10))/2;
                    centerY = (parseInt(y0,10) + parseInt(y, 10))/2;
                    radiusX = (parseInt((document.getElementById('circle').style.width),10))/2;
                    radiusY = (parseInt(document.getElementById('circle').style.height))/2;
                } else if (quadrant == 3) {
                    //console.log(document.clientX);
                    centerX = (document.documentElement.clientWidth - parseInt((document.getElementById('circle').style.right),10) + parseInt(x, 10))/2;
                    centerY = (parseInt((document.getElementById('circle').style.top),10) + parseInt(y, 10))/2;
                    radiusX = (parseInt((document.getElementById('circle').style.width),10))/2;
                    radiusY = (parseInt(document.getElementById('circle').style.height))/2;
                } else  if (quadrant == 4){ 
                    centerX = (parseInt((document.getElementById('circle').style.left),10) + parseInt(x, 10))/2;
                    centerY = (parseInt((document.getElementById('circle').style.top),10) + parseInt(y, 10))/2;
                    radiusX = (parseInt((document.getElementById('circle').style.width),10))/2;
                    radiusY = (parseInt(document.getElementById('circle').style.height))/2;
                }
                console.log('cxy: '+ centerX + " "+ centerY);
                console.log('rxy' + radiusX + " " + radiusY);
                context.ellipse(centerX, centerY, radiusX, radiusY, 0, 0, 2 * Math.PI);
                context.fillStyle = document.getElementById('Color').value;
                context.fill();
                //context.lineWidth = 5;
                //context.strokeStyle = 'red';
                //context.stroke();
                cc.remove();
                isDrawing = false;
            }
        } else if (mode[Mode] == 'SQUARE'){
            if(isDrawing === true){
                context.beginPath();
                if(quadrant == 1){

                    centerX = parseInt((document.getElementById('square').style.left),10);
                    centerY = parseInt(y, 10);
                    radiusX = parseInt((document.getElementById('square').style.width),10);
                    radiusY = parseInt(document.getElementById('square').style.height);
                    
                } else if (quadrant == 2) {
                    centerX = parseInt(x, 10);
                    centerY = parseInt(y, 10);
                    radiusX = parseInt((document.getElementById('square').style.width),10);
                    radiusY = parseInt(document.getElementById('square').style.height);
                } else if (quadrant == 3) {
                    //console.log(document.clientX);
                    centerX = parseInt(x, 10);
                    centerY = parseInt((document.getElementById('square').style.top),10) ;
                    radiusX = parseInt((document.getElementById('square').style.width),10);
                    radiusY = parseInt(document.getElementById('square').style.height);
                } else  if (quadrant == 4){ 
                    centerX = parseInt((document.getElementById('square').style.left),10);
                    centerY = parseInt((document.getElementById('square').style.top),10);
                    radiusX = parseInt((document.getElementById('square').style.width),10);
                    radiusY = parseInt(document.getElementById('square').style.height);
                }
                console.log('cxy: '+ centerX + " "+ centerY);
                console.log('WH' + radiusX + " " + radiusY);
                context.rect(centerX, centerY, radiusX, radiusY);
                context.fillStyle = document.getElementById('Color').value;
                context.fill();
           
                qq.remove();
                isDrawing = false;
            }
        } else if (mode[Mode] == 'TRIANGLE'){
            if(isDrawing === true){
                console.log('md-triangle');
                context.beginPath();
                if(quadrant == 1){
                    
                    topX = (parseInt(x,10) + parseInt(x0,10))/2;
                    topY = parseInt(y,10) ;
                    rightX = parseInt(x,10) ;
                    rightY = parseInt(y0,10) ;
                    leftX = parseInt(x0, 10);
                    leftY = parseInt(y0, 10);
                    
                } else if (quadrant == 2) {
                    topX = (parseInt(x,10) + parseInt(x0,10))/2;
                    topY = parseInt(y,10) ;
                    rightX = parseInt(x0,10) ;
                    rightY = parseInt(y0,10) ;
                    leftX = parseInt(x, 10) ;
                    leftY = parseInt(y0, 10) ;
                } else if (quadrant == 3) {
                    //console.log(document.clientX);
                    topX = (parseInt(x,10) + parseInt(x0,10))/2 ;
                    topY = parseInt(y0,10) ;
                    rightX = parseInt(x0,10) ;
                    rightY = parseInt(y,10) ;
                    leftX = parseInt(x, 10);
                    leftY = parseInt(y, 10) ;
                } else  if (quadrant == 4){ 
                    topX = (parseInt(x,10) + parseInt(x0,10))/2;
                    topY = parseInt(y0,10) ;
                    rightX = parseInt(x,10) ;
                    rightY = parseInt(y,10) ;
                    leftX = parseInt(x0,10) ;
                    leftY = parseInt(y, 10) ;
                }
                

                context.beginPath();
                context.moveTo(topX, topY);
                context.lineTo(rightX, rightY);
                context.lineTo(leftX, leftY);
                context.fill();

                //context.rect(centerX, centerY, radiusX, radiusY);
                context.fillStyle = document.getElementById('Color').value;
                context.fill();
           
                tt.remove();
                isDrawing = false;
            }
        }
    }
    enter_pressed = function(e){
        if(e.key == 'Enter'){
            // 輸入文字
            if(mode[Mode] == 'TEXT'){
            
                context.textAlign = "left";
                context.textBaseline = "top";
                
                //context.fillStyle = "Red";
                context.fillStyle = document.getElementById('Color').value;
                let tmps = document.getElementById('myRange').value;
               
                var tmpt = document.getElementById('textbox').value; 
                
                
                let fontType = document.getElementById('font_selector').value;
                let size = tmps + 'px';
                console.log('font-size is: '+ size);
                console.log('font-type is: ' + fontType);
                context.font = size + ' ' + fontType;
                console.log(context.font);
                // PROBLEM!!!!!!! Don't know offset's value

                context.fillText(tmpt, textpositionX, textpositionY);
                tx.remove();//remove text box
            }
        }
        
    }
    // 呼叫EventListener
    canvas.addEventListener('mousedown', mouse_down);   
    canvas.addEventListener('mousemove', mouse_move);
    window.addEventListener('mouseup', mouse_up);
    try{
       window.addEventListener('keyup', enter_pressed);

    } catch { console.log('no print text')};
    
//-----------------------------------------------------------------------
    // BRUSH draw
    function drawLine(context, x1, y1, x2, y2) {
        //console.log('in drawline');
        context.beginPath();
        //context.strokeStyle = 'black';
        context.strokeStyle = document.getElementById('Color').value;
        context.lineWidth = document.getElementById('myRange').value;
        context.lineCap = "round";
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
        context.closePath();
    }
    // ERASER draw
    function eraseLine(context, x1, y1, x2, y2) {
        //console.log('in erase draw');
        let width = document.getElementById('myRange').value;
        //context.lineCap = "round";
        context.clearRect(x1,y1,width,width);
    }
    
    function drawSquare(context, x1, y1, x2, y2){
        context.fillStyle = document.getElementById('Color').value;
        context.fillRect(x1, y1, x2, y2);
    }
    
}
//=========================================================================
//=========================================================================
// save point
var imgData;

function save_point(){
    console.log('in copy');
    imgData = context.getImageData(0,0, canvas.width,canvas.height);
    undoStack.push(imgData);
    console.log(undoStack.length);
}
function undo(){
    if(undoStack.length){
        //FOR REDO
        imgData = context.getImageData(0,0, canvas.width,canvas.height);; 
        redoStack.push(imgData);
        //UNDO
        imgData = undoStack.pop();
        console.log(undoStack.length);
        //UNDO
        context.putImageData(imgData,0,0);
    } else {
        console.log('undo fail');
    }
    
}
function redo(){
    if(redoStack.length){
        imgData = imgData = context.getImageData(0, 0, canvas.width, canvas.height);
        undoStack.push(imgData);
        imgData = redoStack.pop();
        context.putImageData(imgData,0,0);
    } else {
        console.log('redo fail');
    }
}
function refresh(){
    console.log('refresh.');
    context.putImageData(blankCanvas,0,0);
}

//筆刷大小
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
  //context.lineWidth = this.value;
}
//生成文字方塊
function create_TextBox(tx_x, tx_y){
    //console.log(e); 
    
    //create new text box
    tx = document.createElement("INPUT");
    tx.setAttribute("type", "text");
    tx.setAttribute("id", "textbox");
    tx.setAttribute("style", 
                    "position:absolute; left:" + tx_x + "px; top:" + tx_y + "px; width: 100px");
    tx.setAttribute("autofocus", "");
    document.body.appendChild(tx);
    //tx.focus();
  
}

//顯示圓
function display_circle(x0, y0){
    console.log('displaying circle.');
    cc = document.createElement("DIV");
    //tx.setAttribute("", "text");
    cc.setAttribute("id", "circle");
    let cl =document.getElementById('Color').value;
    console.log(cl);
    
    cc.setAttribute("style", 
                    "position:absolute; left:" + x0 + "px; top:" + y0 + "px; width: 0px ;height: 0px ;background: " +cl);
    //tx.setAttribute("autofocus", "");
    //cc.style.backbround = document.getElementById('Color').value;
    document.body.appendChild(cc);
    
  }
//顯示方形
function display_square(x0, y0){
    console.log('displaying square.');
    qq = document.createElement("DIV");
    //tx.setAttribute("", "text");
    qq.setAttribute("id", "square");
    let ql =document.getElementById('Color').value;
    
    
    qq.setAttribute("style", 
                    "position:absolute; left:" + x0 + "px; top:" + y0 + "px; width: 1px ;height: 1px ;background: " +ql);
    //tx.setAttribute("autofocus", "");
    //cc.style.backbround = document.getElementById('Color').value;
    document.body.appendChild(qq);
}
//顯示三角形
function display_triangle(x0, y0){
    console.log('displaying triangle.');
    tt = document.createElement("DIV");
    //tx.setAttribute("", "text");
    tt.setAttribute("id", "triangle");
    let tl = document.getElementById('Color').value;
    
    
    tt.setAttribute("style", 
                    "position:absolute; left:" + x0 + "px; top:" + y0 + "px; border-bottom: 2px solid " +tl + "; border-left: 1px solid transparent; border-right: 1px solid transparent; " );
    
    document.body.appendChild(tt);
}
  
// upload picture
function readURL(input){

    //if(input.files && input.files[0]){
        
        var imageTagID = input.getAttribute("targetID");
  
        var reader = new FileReader();
  
        reader.onload = function (e) {
  
            //var img = document.getElementById(imageTagID);
            var newimg = new Image();
            var img = document.getElementById('abc');
  
            img.src = e.target.result;
            newimg.src = e.target.result;
            //img.setAttribute("src", e.target.result);
            console.log('here: '+e.target.result);
            //canvas.width = img.width;
            //canvas.height = img.height;
            
            save_point();
            console.log('now upload image');
            newimg.onload = function(){
                
                    console.log(document.getElementById("myRange").value);
                    context.drawImage(newimg, 0, 0, document.getElementById("myRange").value/50*(canvas.width), document.getElementById("myRange").value/50*(canvas.height));
                    console.log('upload accept');
                     
            }

            
        }
        reader.readAsDataURL(input.files[0]);
    //}
}

function download() {
    var download = document.getElementById("download");
    var image = document.getElementById("background").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
    //download.setAttribute("download","archive.png");
}
//cursor setting
function cursor_icon(mode){
    console.log('texticonget');
    if(mode == 0){
        canvas.setAttribute("class", "auto");
    }
    else if(mode == 1){
        canvas.setAttribute("class", "painter");
    }
    else if (mode == 2){
        canvas.setAttribute("class", "eraser");
    }
    else if(mode == 3){
        canvas.setAttribute("class", "text");
    } else if(mode == 4 || mode == 5 || mode == 6) {
        canvas.setAttribute("class", "cross");
    }
}