# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y        |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.


![](https://i.imgur.com/3bREEqH.png)

#### Color

Color是一個input，type=color，用來選擇畫筆、圖形、和文字的顏色。

#### Size

Size是一個input，type=range，拖曳即可改變筆刷和文字大小。

#### Font Family

Font Family是一個select，可以選擇的字型有6種，分別是Arial、Cursor New、Fixedsys、Lucida Sans Unicode、Roman、verdana 。

#### Painter

Painter是一個button，點選之後可以在canvas的區域畫畫。 使用lineTo和moveTo就能實現筆刷的功能，在canvas拖曳滑鼠即可使用筆刷畫畫。 筆刷在拖曳出canvas外的時候並不會停止，只有放開滑鼠的時候才會停止。

#### Eraser

Eraser是一個button，點選之後在canvas上實現橡皮擦的功能 ，拖曳滑鼠即可消除掉筆刷，圖形，文字。 轉換成橡皮擦的時候，需要將global composite operation設成'destination-out'，轉換成其他tools的時候則需要將global composite operation設成'source-over'。 橡皮擦在拖曳出canvas外的時候並不會停止，只有放開滑鼠的時候才會停止。

#### Text Input

Text Input是一個button，點選之後可以在canvas的區域輸入文字。 滑鼠點擊canvas的時候，會創建一個input，type=text，position : absolute，然後把這個element append到document上，利用事件的clientX和clientY實現將文字方塊生成於滑鼠點擊的位置。當使用者輸入Enter的時候，就會把使用者輸入在input element上的文字使用fillText印在canvas上，並把input element從document移除。

#### Circle

Circle是一個button，點選之後可以在canvas的區域上畫實心圓，抑或是實心橢圓。在canvas點擊就會紀錄初始的位置，並在document生成一個div物件，在拖曳的時候會改變此物件的大小，滑鼠放開時會計算結束的位置，以這兩點的距離來計算中心點和半徑，接著使用ellipse(centerX, centerY, radiusX, radiusY)，將實心圓印在canvas上，並把div物件從document中移除，就能實現畫圓的功能。。圓在拖曳出canvas外的時候並不會停止，只有放開滑鼠的時候才會停止。

#### Triangle

Triangle是一個button，點選之後可以在canvas的區域上畫實心三角形。 在canvas點擊就會紀錄初始的位置，並在document生成一個div物件，在拖曳的時候會改變此物件的大小，滑鼠放開時會計算結束的位置，接著使用lineTo和moveTo將三角形印在canvas上，並把div物件從document中移除，就能實現畫三角形的功能。三角形在拖曳出canvas外的時候並不會停止，只有放開滑鼠的時候才會停止。

#### Rectangle

Rectangle是一個button，點選之後可以在canvas的區域上畫實心長方形。在canvas點擊就會紀錄初始的位置，並在document生成一個div物件，在拖曳的時候會改變此物件的大小，滑鼠放開時會計算結束的位置，接著使用fillRect()將長方形印在canvas上，並把div物件從document中移除，就能實現畫長方形的功能。長方形在拖曳出canvas外的時候並不會停止，只有放開滑鼠的時候才會停止。

#### Undo

Undo是一個button，點選之後會回到上一個canvas的狀態。當滑鼠在canvas上點擊的時候，會先把當前canvas的狀態以getImageData 的方式save下來，以圖片的形式push到undo Stack。當點到undo button的時候，如果undo list裡面有element，則把目前的canvas以圖片的形式push到redo Stack，同時從undo Stack裡面pop出上一個canvas的照片，然後以putImageData的形式把這個照片畫在目前的canvas上，這樣就能完成undo的功能。

#### Redo

Redo是一個button，點選之後如果redo Stack裡面有element，就把目前的canvas以圖片的形式push到undo Stack，同時從redo Stack裡面pop出undo前canvas的照片，然後以putImageData的形式把這個額照片畫在目前的canvas上，這樣就能完成恢復撤銷的功能。

#### Download

Download是一個button，點選之後會把當前的canvas下載下來。

#### Upload

Upload是一個input，type=file，accept=image/*，點選之後可以上傳一張照片印到canvas中，圖片長寬由Size Input決定。

#### Refresh

Refresh是一個button，點選之後會把canvas清空，並可以透過Undo還原上一步。

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

https://108000203.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

